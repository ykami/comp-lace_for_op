# -*- coding: utf-8 -*-

__author__ = 'Y. Kamijima'
__date__ = '2020/02/25'

from abc import ABCMeta, abstractmethod
from enum import Enum
import itertools
import math
import numpy as np
import scipy.integrate
import scipy.special
import sys
from typing import Dict, List, Tuple

# 小数第 numDigits で切り上げる。
def DecimalCeil(value: float, numDigits: int):
    temp = value * 10 ** numDigits
    temp = math.ceil(temp)
    return temp / 10 ** numDigits

# Multinomial coefficient.
# Ref: https://stackoverflow.com/questions/46374185/does-python-have-a-function-which-computes-multinomial-coefficients
def Multinomial(params: List[int]) -> int:
    if len(params) == 1:
        return 1
    else:
        return scipy.special.comb(np.sum(params), params[-1], exact=True) * Multinomial(params[:-1])

# Ref: https://stackoverflow.com/questions/37711817/generate-all-possible-outcomes-of-k-balls-in-n-bins-sum-of-multinomial-catego
def Multichoose(N: int, k: int) -> List[Tuple[float]]:
    if N < 0 or k < 0:
        raise ValueError('N and k must be non-negative')
    elif k == 0:
        return
    elif N == 0:
        yield (0,) * k
    else:
        masks = np.identity(k, dtype=np.int)
        for lst in itertools.combinations_with_replacement(masks, N):
            yield tuple(sum(lst))

class LatticeStructure(Enum):
    SimpleCubic = 0
    BodyCenteredCubic = 1

class RandomWalk:
    _instance = None
    def __new__(cls):
        raise NotImplementedError('Cannot initialize via Constructor')

    @classmethod
    def __internal_new__(cls):
        return super().__new__(cls)

    @classmethod
    def GetInstance(cls):
        if not cls._instance:
            cls._instance = cls.__internal_new__()
            cls.__init__(cls)
        return cls._instance

    def __init__(self):
        self.__NumStored: int = 5
        self._dimension: int = 0
        self.Loop: List[float] = np.zeros(self.__NumStored, dtype=np.float64)
        self.Bubble: List[float] = np.zeros(self.__NumStored, dtype=np.float64)

    def GetNumStored(self):
        return self.__NumStored

    @abstractmethod
    def ReturnProbability(self, numSteps: int) -> float:
        pass

    # The supremum of the transition probability D^{\ast n}(x) over x.
    def TransitionProbability(self, numSteps: int) -> float:
        if numSteps < 0:
            return 0.e0
        elif numSteps == 0:
            return 1.e0
        elif numSteps == 1:
            return 0.5e0 ** self._dimension
        elif numSteps % 2 == 0:
            return self.ReturnProbability(numSteps)
        else:
            return self.ReturnProbability(numSteps - 1)

    def Reset(self, dimension: int):
        if self._dimension != dimension:
            self._dimension = dimension
            self.Loop = np.array([self._calcLoop(i) for i in range(self.__NumStored)])
            self.Bubble = np.array([self._calcBubble(i) for i in range(self.__NumStored)])

    def Compute(self) -> Dict[str, float]:
        result = {'RW return prob.(' + str(i) + ')': self.ReturnProbability(i) for i in range(0, self.__NumStored + self.__NumStored // 2, 2)}
        result.update({'RW trans. prob.(' + str(i) + ')': self.TransitionProbability(i) for i in range(0, self.__NumStored)})
        result.update({'RW loop(' + str(i) + ')': self.Loop[i] for i in range(0, self.__NumStored)})
        result.update({'RW bubble(' + str(i) + ')': self.Bubble[i] for i in range(0, self.__NumStored)})
        return result

    # 経路全体の最小の長さの半分をhalfOfLeastStepsとする。
    @abstractmethod
    def _calcLoop(self, halfOfLeastSteps: int) -> float:
        pass

    # percolationと異なる。
    @abstractmethod
    def _calcBubble(self, halfOfLeastSteps: int) -> float:
        pass

class RandomWalkOnBCC(RandomWalk):
    def ReturnProbability(self, numSteps: int) -> float:
        def oneDimensional(n: int) -> float:
            if n % 2 == 1 or n < 0:
                return 0.e0
            elif n == 0:
                return 1.e0
            else:
                return oneDimensional(n - 2) * (n - 1) / n  # An suitable expression to the simulation.
                #return math.comb(n, n // 2) / 2 ** n  # The exact expression.  The function comb is added in ver. 3.8.
                #return scipy.special.comb(n, n // 2, exact=True) / 2 ** n
        return oneDimensional(numSteps) ** self._dimension

    def _calcLoop(self, halfOfLeastSteps: int) -> float:
        if self._dimension <= 2:
            return np.inf
        TrialTimes: int = 500
        temp: float = np.sum(np.array([self.ReturnProbability(2 * (halfOfLeastSteps + i)) for i in range(TrialTimes)], dtype=np.float64))
        temp += 1.e0 / (math.pi * (TrialTimes + halfOfLeastSteps)) ** (self._dimension * 0.5e0)\
                + 2.e0 * (TrialTimes + halfOfLeastSteps) / (self._dimension - 2)\
                        / (math.pi * (TrialTimes + halfOfLeastSteps)) ** (self._dimension * 0.5e0)
        return temp

    def _calcBubble(self, halfOfLeastSteps: int) -> float:
        if self._dimension <= 4:
            return np.inf
        TrialTimes: int = 500
        temp: float = np.sum(np.array([(i + 1) * self.ReturnProbability(2 * (halfOfLeastSteps +  i)) for i in range(TrialTimes)], dtype=np.float64))
        temp += (TrialTimes + halfOfLeastSteps) / (math.pi * (TrialTimes + halfOfLeastSteps)) ** (self._dimension * 0.5e0)\
                + 2.e0 * (TrialTimes + halfOfLeastSteps) ** 2 / (self._dimension - 4)\
                        / (math.pi * (TrialTimes + halfOfLeastSteps)) ** (self._dimension * 0.5e0)\
                - 2.e0 * (halfOfLeastSteps - 1) * (TrialTimes + halfOfLeastSteps) / (self._dimension - 2)\
                        / (math.pi * (TrialTimes + halfOfLeastSteps)) ** (self._dimension * 0.5e0)
        return temp

class RandomWalkOnSC(RandomWalk):
    def ReturnProbability(self, numSteps: int) -> float:
        temp: float = 0.5e0 / self._dimension
        if numSteps % 2 == 1 or numSteps < 0:
            return 0.e0
        elif numSteps == 0:
            return 1.e0
        elif numSteps == 2:
            return temp
        elif numSteps == 4:
            return 3 * temp ** 2 - 3 * temp ** 3
        elif numSteps == 6:
            return 15 * temp ** 3 - 45 * temp ** 4 + 40 * temp ** 5
        elif numSteps == 8:
            return 105 * temp ** 4 - 630 * temp ** 5 + 1435 * temp ** 6 - 1155 * temp ** 7
        else:
            return np.sum(np.array([
                Multinomial(choice) * np.prod(np.frompyfunc(lambda i: scipy.special.factorial2(i - 1) / scipy.special.factorial2(i), 1, 1)(np.array(choice, dtype=np.int)))
                if np.all(np.array(choice, dtype=np.int) % 2 == 0) else 0.e0
                for choice in Multichoose(numSteps, self._dimension)],  # ここの処理が重い。
                dtype=np.float64)) / self._dimension ** numSteps

    def _calcLoop(self, halfOfLeastSteps: int) -> float:
        return self.__smallParameter(1, halfOfLeastSteps)

    def _calcBubble(self, halfOfLeastSteps: int) -> float:
        return self.__smallParameter(2, halfOfLeastSteps)

    # \varepsilon_i^{(\nu)}
    def __smallParameter(self, i: int, n: int) -> float:

        def modifiedGreenFunction(power: int) -> float:
            if power < 0:
                return 0.e0
            elif power == 0:
                return 1.e0
            elif power == 1:
                return self.__greenFunction(1)
            elif power == 2:
                return 0.5e0 * (self.__greenFunction(2) + self.__greenFunction(1))
            else:
                raise ValueError('unexpected value: ' + str(power))

        temp: float = np.sum(np.array([
            scipy.special.comb(n, j, exact=True) * (-1) ** j * modifiedGreenFunction(i - j)
            for j in range(i + 1)],
            dtype=np.float64))
        temp += np.sum(np.array([
            scipy.special.comb(n, j, exact=True) * scipy.special.comb(j - i, l, exact=True) * (-1) ** (l + j) * self.ReturnProbability(2 * l)
            for j in range(i + 1, n + 1) for l in range(j - i + 1)],
            dtype=np.float64))
        return temp

    # S_1^{\ast n}(o)
    def __greenFunction(self, convolutionPower: int) -> float:
        Eps: float = 1.49e-16  # 数値積分の許容誤差。
        if convolutionPower < 0:
            raise ValueError('the convolution power must be non-negative')
        elif convolutionPower == 0:
            return 1.e0
        else:
            integrand = lambda s: s ** (convolutionPower - 1) * scipy.special.ive(0, s) ** self._dimension
            value, error = scipy.integrate.quad(integrand, 0.e0, np.inf, epsabs=Eps, epsrel=Eps)  # 次元が非常に大きいときには誤差が大きくなる。
            #sys.stderr.write("integration error: {}\n".format(error))
            return self._dimension ** convolutionPower * value / scipy.special.factorial(convolutionPower - 1, exact=True)

class BasicDiagrams:
    latticeStructure: LatticeStructure = LatticeStructure.BodyCenteredCubic

    # weakBoundsと各OPの量との対応？
    def __init__(self, dimension: int, weakBounds: List[float]):
        self.__dimension: int = dimension
        self.__weakBounds: List[float] = weakBounds
        self.__randomWalk: RandomWalk = None
        self.SetLatticeStructure()
        self.__randomWalk.Reset(dimension)

    # leastLength, leftLeastLength, rightLeastLengthなどはすべて自然数。
    def LineWith(self, leastLength: int) -> float:
        return self.__weakBounds[0] ** leastLength * self.__weakBounds[1] * math.sqrt(self.__randomWalk.Loop[leastLength])

    def BubbleWith(self, leastLength: int) -> float:
        return self.__weakBounds[0] ** leastLength * self.__weakBounds[1] ** 2 * self.__randomWalk.Loop[leastLength // 2]

    def TriangleWith(self, leastLength: int) -> float:
        return math.sqrt(2) * self.__weakBounds[0] ** leastLength * self.__weakBounds[1] ** 3 * self.__randomWalk.Bubble[leastLength // 2]

    def WeightedBubble(self, leftLeastLength: int, rightLeastLength: int) -> float:
        if leftLeastLength == 1 and rightLeastLength == 1:
            return self.__weakBounds[0] ** 2 * self.__randomWalk.TransitionProbability(1) + self.LineWith(3) + self.WeightedBubble(2, 1)
        else:
            leastLength = leftLeastLength + rightLeastLength
            return leftLeastLength * (leftLeastLength - 1) * self.__weakBounds[0] * self.BubbleWith(leastLength - 1)\
                    + leftLeastLength * self.__weakBounds[0] ** (leastLength - 1) * self.__weakBounds[1] * self.__weakBounds[2]\
                    * (math.sqrt(2) + 4) * self.__randomWalk.Bubble[(leastLength - 1) // 2]

    def DifferentiatedBubble(self, leftLeastLength: int, rightLeastLength: int) -> float:
        if leftLeastLength == 1 and rightLeastLength == 1:
            return self.__dimension * (self.LineWith(2) + 2 * self.BubbleWith(3)\
                    + 10 * math.sqrt(2) * self.__weakBounds[0] ** 2 * self.__weakBounds[1] * self.__weakBounds[3]
                    * self.__randomWalk.Bubble[2])
        else:
            leastLength = leftLeastLength + rightLeastLength
            return self.__dimension * (leftLeastLength * (leftLeastLength - 1) * self.BubbleWith(leastLength)\
                    + self.__weakBounds[0] ** ((leastLength - 1) // 2) * self.__weakBounds[1] * self.__weakBounds[3]\
                    * 5 * math.sqrt(2) * leftLeastLength * self.__randomWalk.Bubble[(leastLength - 1) // 2])

    def Reset(self, dimension: int, weakBounds: List[float]):
        self.__dimension = dimension
        self.__weakBounds = weakBounds
        self.__randomWalk.Reset(dimension)

    def SetLatticeStructure(self):
        if self.latticeStructure == LatticeStructure.SimpleCubic:
            self.__randomWalk = RandomWalkOnSC.GetInstance()
        elif self.latticeStructure == LatticeStructure.BodyCenteredCubic:
            self.__randomWalk = RandomWalkOnBCC.GetInstance()
        else:
            raise ValueError('Illegal type: ' + latticeStructure.name)

    def Compute(self) -> Dict[str, float]:
        result = {'Line(' + str(i) + ')': self.LineWith(i) for i in range(0, self.__randomWalk.GetNumStored(), 2)}
        result.update({'Bubble(' + str(i) + ')': self.BubbleWith(i) for i in range(0, self.__randomWalk.GetNumStored(), 2)})
        result.update({'Triangle(' + str(i) + ')': self.TriangleWith(i) for i in range(0, self.__randomWalk.GetNumStored(), 2)})
        result.update({'Weighted closed bubble': self.WeightedBubble(2, 2)})
        result.update({'Weighted open bubble(1, 1)': self.WeightedBubble(1, 1)})
        result.update({'Weighted open bubble(1, 2)': self.WeightedBubble(1, 2)})
        result.update({'Weighted open bubble(2, 1)': self.WeightedBubble(2, 1)})
        result.update({'Weighted open bubble(1, 3)': self.WeightedBubble(1, 3)})
        result.update({'Weighted open bubble(3, 1)': self.WeightedBubble(3, 1)})
        #result.update({'Second moment of open bubble(1, 1)': self.DifferentiatedBubble(1, 1)})
        #result.update({'Second moment of open bubble(1, 3)': self.DifferentiatedBubble(1, 3)})
        result.update(self.__randomWalk.Compute())
        return result

class Perturbation:  # The lace expansion coefficietns.
    def __init__(self, dimension: int, weakBounds: List[float]):
        self.__dimension: int = dimension
        self.__weakBounds: List[float] = weakBounds
        self.__basicDiagrams: BasicDiagrams = BasicDiagrams(dimension, weakBounds)

    # The sum of the Fourier 0-mode of the lace expansion coefficietns over even parts.
    # \sum_{N=0}^{\infty}\sum_{(x, t)} \pi_p^{(2N)}(x, t) m^t
    def EvenLaceZero(self) -> float:
        def B(length):
            return self.__basicDiagrams.BubbleWith(length)
        def T(length):
            return self.__basicDiagrams.TriangleWith(length)
        pi2 = 3 * B(4) ** 2 + 2 * B(3) * T(4)\
                + B(4) ** 2 * T(3) + 2 * B(3) * T(2) * T(3)
        return pi2 + (B(2) + 0.5e0 * B(4) ** 2 + T(2) * B(2))\
                * 8 * T(2) ** 3 / (1 - 4 * T(2) ** 2)

    # The sum of the Fourier 0-mode of the lace expansion coefficietns over odd parts.
    # \sum_{N=0}^{\infty}\sum_{(x, t)} \pi_p^{(2N+1)}(x, t) m^t
    def OddLaceZero(self) -> float:
        def B(length):
            return self.__basicDiagrams.BubbleWith(length)
        def T(length):
            return self.__basicDiagrams.TriangleWith(length)
        pi1MinusPi0 = 0.5e0 * B(4) + B(4) * B(2) + 4.5e0 * B(4) ** 2 + 3 * B(3) * T(4)
        return pi1MinusPi0 + (B(2) + 0.5e0 * B(4) ** 2 + T(2) * B(2))\
                * 4 * T(2) ** 2 / (1 - 4 * T(2) ** 2)

    # \sum_{N=0}^{\infty}\sum_{(x, t)} \pi_p^{(N)}(x, t) m^t t
    def LaceWithEmbeddedPoint(self) -> float:
        def B(length):
            return self.__basicDiagrams.BubbleWith(length)
        def T(length):
            return self.__basicDiagrams.TriangleWith(length)
        pi1MinusPi0 = 0.5e0 * (B(4) + T(4)) + B(4) * (B(2) + T(2))\
                + 1.5e0 * B(4) * (2 * B(4) + T(4))\
                + 6 * B(4) * (B(4) + T(4)) + 3 * (T(3) * T(4) + B(3) * T(4) + T(4) * T(3))
        pi2 = B(4) * (2 * B(4) + T(4))\
                + 4 * B(4) * (B(4) + T(4)) + 2 * (T(3) * T(4) + B(3) * T(4) + T(4) * T(3))\
                + B(4) * (2.5e0 * B(4) * T(3) + 2 * T(4) * T(3))\
                + (6 * T(3) ** 2 * T(2) + B(3) * T(2) * T(3))
        general = (T(2) + B(4) ** 2 + 0.5e0 * B(4) * T(4) + 2 * T(2) ** 2) * 4 * T(2) ** 2 / (1 - 2 * T(2))\
                + (T(2) + 0.5e0 * B(4) * T(4) + T(2) ** 2) * 2 * T(2) * (1.e0 / (1 - 2 * T(2)) + 2 * T(2)) / (1 - 2 * T(2))
        return pi1MinusPi0 + pi2 + general

    # The sum of the difference between the Fourier k-mode and 0-mode of the lace expansion coefficietns.
    # \sum_{N=0}^{\infty}\sum_{(x, t)} \pi_p^{(N)}(x, t) m^t \frac{1 - \cos k\cdot x}{1 - \hat D(k)}
    def LaceDifference(self) -> float:
        def B(length):
            return self.__basicDiagrams.BubbleWith(length)
        def T(length):
            return self.__basicDiagrams.TriangleWith(length)
        def V(leftLength, rightLength):
            return self.__basicDiagrams.WeightedBubble(leftLength, rightLength)
        pi1MinusPi0 = 0.5e0 * V(2, 2) + 2 * (V(2, 2) * B(2) + B(4) * V(1, 2))\
                + 1.5e0 * B(4) * V(3, 1)\
                + 12 * B(4) * V(2, 2) + 6 * (V(1, 2) * T(4) + T(4) * V(2, 1))
        pi2 = B(4) * V(3, 1)\
                + 8 * B(4) * V(2, 2) + 4 * (V(1, 2) * T(4) + T(4) * V(2, 1))\
                + B(4) * (2 * V(3, 1) * T(3) + T(4) * V(2, 1) + T(4) * V(1, 2))\
                + 3 * (V(1, 2) * T(2) * T(3) + T(3) * V(1, 1) * T(3) + T(3) * T(2) * V(2, 1) + T(3) * T(2) * V(1, 2))
        general = (V(1, 1) + 0.5e0 * B(4) * V(1, 3) + 2 * T(2) * V(1, 1)) * 8 * T(2) ** 2 * (2 - 3 * T(2)) / (1 - 2 * T(2)) ** 2\
                + (T(2) + 0.5e0 * B(4) * T(4) + T(2) ** 2) * 2 * V(1, 1)\
                * (8 * T(2) * (1 - T(2)) / (1 - 2 * T(2)) ** 3 + 4 * T(2) * (2 - 3 * T(2)) / (1 - 2 * T(2)) ** 2)
        return pi1MinusPi0 + pi2 + general

    # The sum of the second moment of the lace expansion coefficietns.  This bound is almost same as LaceDiffernce.
    # \sum_{N=0}^{\infty}\sum_{(x, t)} \pi_p^{(N)}(x, t) m^t \lVert x\rVert_2^2
    def LaceSecondMoment(self) -> float:
        def L(length):
            return self.__basicDiagrams.LineWith(length)
        def B(length):
            return self.__basicDiagrams.BubbleWith(length)
        def T(length):
            return self.__basicDiagrams.TriangleWith(length)
        def V(leftLength, rightLength):
            return self.__basicDiagrams.DifferentiatedBubble(leftLength, rightLength)
        pi0 = 0.5e0 * V(2, 2)
        pi1 = V(2, 2) * L(4)\
                + self.__weakBounds[0] * B(4) * L(3)\
                + 2 * V(1, 2) * B(4)\
                + 2 * V(2, 2) * B(3)\
                + V(2, 2)\
                + V(4, 2) * B(4)\
                + 2 * V(1, 1) * B(4)\
                + 2 * V(2, 2) * B(2)
        general = (V(1, 1) + 0.5e0 * V(1, 3) * B(4) + 2 * V(1, 1) * T(2)) * 2 * T(2) * (3 - 4 * T(2)) / (1 - 2 * T(2)) ** 2\
                + (T(2) + 0.5e0 * B(4) * T(4) + T(2) ** 2) * 2 * V(1, 1)\
                * (8 * (1 - T(2)) * T(2) / (1 - 2 * T(2)) ** 3 + (3 - 4 * T(2)) / (1 - 2 * T(2)) ** 2)
        return pi0 + pi1 + general

    def Reset(self, dimension: int, weakBounds: List[float]):
        self.__dimension = dimension
        self.__weakBounds = weakBounds
        self.__basicDiagrams.Reset(dimension, weakBounds)

    def Compute(self) -> Dict[str, float]:
        result = {'Sum of lace over even': self.EvenLaceZero()}
        result.update({'Sum of lace over odd': self.OddLaceZero()})
        result.update({'Sum of embedded diagram': self.LaceWithEmbeddedPoint()})
        result.update({'Sum of lace diff.': self.LaceDifference()})
        #result.update({'Sum of Laplacian of lace': self.LaceSecondMoment()})
        result.update(self.__basicDiagrams.Compute())
        return result

class BootstrappingFunctions:
    latticeStructure: LatticeStructure = LatticeStructure.BodyCenteredCubic

    def __init__(self, dimension: int, weakBounds: List[float]):
        self.__dimension: int = dimension
        self.__weakBounds: List[float] = weakBounds
        self.__perturbation: Perturbation = Perturbation(dimension, weakBounds)

    def ExternalCondition(self) -> float:
        return 1.e0 / (1.e0 - self.__perturbation.OddLaceZero())

    def TwoPointFunction(self) -> float:
        laceZero = self.__perturbation.EvenLaceZero() + self.__perturbation.OddLaceZero()

        # Discrete version.
        multiplicativeConstant = 1.e0
        if self.latticeStructure == LatticeStructure.SimpleCubic:
            multiplicativeConstant = 4.e0
        elif self.latticeStructure == LatticeStructure.BodyCenteredCubic:
            multiplicativeConstant = 2.e0
        else:
            raise ValueError('Illegal type: ' + latticeStructure.name)
        return (1 + laceZero) / (1 - self.__perturbation.OddLaceZero())\
                + self.__weakBounds[1]\
                        * (2 * self.__perturbation.EvenLaceZero() + self.__weakBounds[0]\
                        # この定数は波数と角振動数のどちらを制限するかに依存．
                        * multiplicativeConstant\
                        * max(math.pi * self.__perturbation.LaceWithEmbeddedPoint(), self.__perturbation.LaceDifference()))\
                / (1 - self.__perturbation.OddLaceZero())

        # Continuous version.
        #return (1 + laceZero) / (1 - self.__perturbation.OddLaceZero())\
        #        + self.__weakBounds[1]\
        #                * (2 * self.__perturbation.EvenLaceZero() + self.__weakBounds[0] * 2 * math.sqrt(2) * math.pi ** 2\
        #                * max(self.__perturbation.LaceWithEmbeddedPoint(), self.__perturbation.LaceSecondMoment() * 0.5e0 / self.__dimension))\
        #        / (1 - self.__perturbation.OddLaceZero())

    def DiscreteLaplacianOfTwoPointFunction(self) -> float:
        laceZero = self.__perturbation.EvenLaceZero() + self.__perturbation.OddLaceZero()
        return max(1, self.TwoPointFunction() / (1 - laceZero)) ** 3 * self.__weakBounds[0] ** 2\
                * (1 + 2 * laceZero + 2 * self.__perturbation.LaceDifference()) ** 2

    def ContinuousLaplacianOfTwoPointFunction(self) -> float:
        laceZero = self.__perturbation.EvenLaceZero() + self.__perturbation.OddLaceZero()
        return 0.2e0 * self.__weakBounds[0] * (1 + 2 * laceZero + 2 * self.__perturbation.LaceSecondMoment())\
                * (1 + 8 * self.__weakBounds[0] * self.TwoPointFunction()\
                        * (1 + 2 * laceZero + 2 * self.__perturbation.LaceDifference()) \
                        / (1 - laceZero))\
                * (self.TwoPointFunction() / (1 - laceZero)) ** 2

    # 条件を満たさない場合は全て0のリストを返す。
    def CheckBounds(self) -> List[float]:
        #result = np.array([self.ExternalCondition(), self.TwoPointFunction(),
        #        self.DiscreteLaplacianOfTwoPointFunction(), self.ContinuousLaplacianOfTwoPointFunction()], dtype=np.float64)
        #if result[0] < self.__weakBounds[0] and result[1] < self.__weakBounds[1]\
        #        and result[2] < self.__weakBounds[2] and result[3] < self.__weakBounds[3]:
        result = np.array([self.ExternalCondition(), self.TwoPointFunction(),
                self.DiscreteLaplacianOfTwoPointFunction()], dtype=np.float64)
        if result[0] < self.__weakBounds[0] and result[1] < self.__weakBounds[1] and result[2] < self.__weakBounds[2]:
            return np.concatenate((self.__weakBounds, result))
        else:
            return np.zeros(len(np.concatenate((self.__weakBounds, result))), dtype=np.float64)

    def Reset(self, dimension: int, weakBounds: List[float]):
        self.__dimension = dimension
        self.__weakBounds = weakBounds
        self.__perturbation.Reset(dimension, weakBounds)

    def Compute(self) -> Dict[str, float]:
        result = {'dimension': self.__dimension}
        result.update({'K1': self.__weakBounds[0]})
        result.update({'K2': self.__weakBounds[1]})
        result.update({'K3': self.__weakBounds[2]})
        #result.update({'K4': self.__weakBounds[3]})
        result.update({'Occupation density': self.ExternalCondition()})
        result.update({'Two-point function': self.TwoPointFunction()})
        result.update({'Discrete Laplacian': self.DiscreteLaplacianOfTwoPointFunction()})
        #result.update({'Continuous Laplacian': self.ContinuousLaplacianOfTwoPointFunction()})
        result.update(self.__perturbation.Compute())
        return result
