#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Y. Kamijima'
__date__ = '2020/06/19'

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from typing import List, Tuple

def OneStepDistribution(wavenumber: List[float]) -> float:
    #return np.sum(np.cos(wavenumber)) / len(wavenumber)  # Simple Cubic lattice.
    return np.prod(np.cos(wavenumber))  # Body-Centered Cubic lattice.

if __name__ == '__main__':
    __dimension = 3
    __numDivisions = 20

    ticks = np.stack([np.linspace(-np.pi, np.pi, __numDivisions) for i in range(__dimension)])
    domain = np.stack(np.meshgrid(*ticks), -1).reshape(-1, __dimension)
    result = np.apply_along_axis(lambda k: OneStepDistribution(k), 1, domain)
    result = np.sign(result)  # 符号のみ見たい。
    output = np.hstack([domain, result.reshape((result.size, 1))])
    del domain, result

    fig = plt.figure()
    if __dimension == 2:
        plt.scatter(output[:, 0], output[:, 1], c=output[:, 2])
    elif __dimension == 3:
        #output = output[output[:, 3] >= 0]  # 非負の領域のみ描画．
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter3D(output[:, 0], output[:, 1], zs=output[:, 2], c=output[:, 3], marker='s')
    plt.show()
