#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from laceExpansion import RandomWalk, RandomWalkOnBCC, RandomWalkOnSC
import numpy as np
import sys
from typing import List

def MakeNumericalTable(dims: List[int], rw: RandomWalk):
    res = np.array([[]], dtype=np.float64).reshape(0, 5)
    for d in dims:
        rw.Reset(d)
        temp = np.concatenate([rw.Loop[1:3].reshape(1, 2), rw.Bubble[1:3].reshape(1, 2)], axis=1)
        temp = np.insert(temp, 0, d)
        res = np.vstack((res, temp))
    return res

if __name__ == '__main__':
    rw = RandomWalkOnBCC.GetInstance()
    res = MakeNumericalTable(list(range(3, 11)), rw)
    np.savetxt(sys.stdout, res, delimiter=',', fmt="%.10e")
