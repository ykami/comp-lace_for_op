#!/usr/bin/bash

for dim in `seq 3 12`
do
	data=$(python main.py -l 1 -d $dim 1.002 1.05 1.25 |
		grep -E 'RW (loop|bubble)' |
		cut -d'=' -f2 |
		tr -d ' ' |
		tr '\n' '&' |
		sed -e's/&$/\\\\\n/g' |
		sed -e's/&/ \& /g' |
		sed -e's/inf/\$\\infty\$/g' |
		sed -e's/\([1-9]\.[0-9]\+e[+-][0-9]\+\)/\\num{\1}/g')
	echo "\\num{$dim} & $data"
done
