#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Y. Kamijima'
__date__ = '2019/12/20'

import argparse
import datetime
import multiprocessing as mp
import numpy as np
from typing import List

from laceExpansion import BootstrappingFunctions, BasicDiagrams, LatticeStructure

# 入力サイズ (NumDivision ** NumParameters) x NumParameters の配列を受け取った上で、その各行をパラメータと見なして上界を確認する。
# 出力サイズは (NumDivision ** NumParameters) x (2 * NumParameters)。
def CheckWhetherBootstrappingWorks(queue: mp.Queue, dimension: int, weakBoundsArray: List[List[float]], lattice: LatticeStructure = LatticeStructure.BodyCenteredCubic) -> List[List[float]]:
    NumParameters = weakBoundsArray.shape[1]
    BasicDiagrams.latticeStructure = BootstrappingFunctions.latticeStructure = lattice  # mainで書き替えても何故かここでは元の値のままなので、bootstrappingFunctionsの生成の直前で書き替える。
    bootstrappingFunctions = BootstrappingFunctions(dimension, np.ones(NumParameters, dtype=np.float64))
    result = np.empty((0, NumParameters * 2), dtype=np.float64)
    for weakBounds in weakBoundsArray:
        bootstrappingFunctions.Reset(dimension, weakBounds)
        bounds = bootstrappingFunctions.CheckBounds()
        if np.all(bounds > 0.e0):
            result = np.append(result, bounds.reshape(1, NumParameters * 2), axis=0)
    queue.put(result)
    return result

def SearchBounds(dimension: int, maxWeakBounds: List[float], lattice: LatticeStructure = LatticeStructure.BodyCenteredCubic) -> List[List[float]]:
    NumDivision = 100
    NumParameters = maxWeakBounds.shape[0]
    RangeOfParameters = [[1, maxWeakBounds[i]] for i in range(NumParameters)]
    NumProcesses = 8

    queue = mp.Queue()
    ticks = np.stack([np.linspace(RangeOfParameters[i][1], RangeOfParameters[i][0], NumDivision, endpoint=False) for i in range(NumParameters)])
    weakBoundsArray = np.stack(np.meshgrid(*ticks), -1).reshape(-1, NumParameters)

    # 並列化しない場合。
    #return CheckWhetherBootstrappingWorks(queue, dimension, weakBoundsArray)

    # 並列化する場合。各プロセスへパラメータの組を適当に振り分ける。
    splitWeakBoundsArray = np.array_split(weakBoundsArray, NumProcesses)
    #with mp.Pool(NumProcesses) as p:
    #    result = p.map(CheckWhetherBootstrappingWorks, splitWeakBoundsArray)
    #return np.concatenate(result)
    processesList = []
    for i in range(NumProcesses):
        process = mp.Process(target=CheckWhetherBootstrappingWorks, args=(queue, dimension, splitWeakBoundsArray[i], lattice))
        process.start()
        processesList.append(process)
    return np.concatenate([queue.get() for i in range(NumProcesses)])

def arbitraryCeil(x, ndigits=0):
    return np.true_divide(np.ceil(x * 10 ** ndigits), 10 ** ndigits)

# xを有効数字ndigits桁で切り上げる。
def ceilBySignificantFigures(x, ndigits):
    return arbitraryCeil(x, ndigits - np.floor(np.log10(np.abs(value))) - 1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=
            'Search ranges of K*\'s for a tuple of weak bounds satisfying the bootstrapping'
            'arguement.  In the default mode, this program outputs some model quantities.  '
            'If you would like to find such tuple of weak bounds, then set a mode `-S\'.')
    parser.add_argument('K1', type=float, help='An weak bound on the occupation density.')
    parser.add_argument('K2', type=float, help='An weak bound on the two-point function.')
    parser.add_argument('K3', type=float, help='An weak bound on the discrete Laplacian of the two-point function.')
    parser.add_argument('-d', '--dimension', type=int, default=5, help='The system dimension.')
    parser.add_argument('-S', '--search', action='store_true',
            help='Search the input ranges for weak bounds.  The parameters K* are interpreted as the range of the weak bounds.')
    parser.add_argument('-l', '--lattice_structure', type=int, default=1, help='The type of the lattice structure.  SC=0, BCC=1.')
    args = parser.parse_args()
    np.set_printoptions(formatter={'float': '{: 9.6e}'.format}, linewidth=150, threshold=6 * 10)

    if args.search:
        lattice = LatticeStructure(args.lattice_structure)
        condition = 'dimension: {0}; range of K1: {1}--{2}; range of K2: {3}--{4}; range of K3: {5}--{6}.'.format(args.dimension, 1.0, args.K1, 1.0, args.K2, 1.0, args.K3)
        print('#', lattice.name, 'lattice.')
        print('#', condition)
        result = SearchBounds(args.dimension, np.array([args.K1, args.K2, args.K3], dtype=np.float64), lattice)
        if result.size > 0:
            np.savetxt('output.ssv', result, fmt='%.7e', delimiter=' ', header='date: ' + datetime.datetime.now().isoformat() + '.\n' + lattice.name + ' lattice.\n' + condition)
            print(result)
    else:
        BasicDiagrams.latticeStructure = BootstrappingFunctions.latticeStructure = LatticeStructure(args.lattice_structure)
        bootstrappingFunctions = BootstrappingFunctions(args.dimension, np.array([args.K1, args.K2, args.K3], dtype=np.float64))
        for key, value in bootstrappingFunctions.Compute().items():
            #print(key, '=', value)
            print(key, '=', '{:.9e}'.format(ceilBySignificantFigures(value, 10) if np.isinf(value) == False else value))
